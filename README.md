  this program has two parts, the first one shows the result in the console. The second, uses opengl to show the result in 3D.
  the opengl code works but it's deprecated. So don't use this as an example to write an opengl program!
  
  

about the licence :
===================

  to be honnest, I don't understand the licence, I don't understand laws and stuffs like that... 


check if a point is inside a polygon :
======================================

  http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html


Discrete Poisson equation :
===========================

  http://en.wikipedia.org/wiki/Discrete_Poisson_equation
  (for Discrete Laplace equation set g=0)


known bug :
===========

 The OpenGL window isn't always redrawn, just it a key like "h", "i"...


requirements :
==============

  Both need Armadillo (C++ linear algebra library ) : http://arma.sourceforge.net/
  for the OpenGL Output you need also OpenGL : www.opengl.org/  
  and X11 : http://www.x.org/wiki/


to compil :
===========

  in ConsoleOutput folder :
  $ cmake .
  $ make

  in OpenGLOutput folder :
  $ cmake .
  $ make
