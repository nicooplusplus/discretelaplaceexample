/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *                                                                                  
 */
#ifndef __GRIDXZ_H_INCLUDED__
#define __GRIDXZ_H_INCLUDED__
#include <vector>
#include <map>
#include <armadillo>
#include<GL/glx.h>


enum Remove {Inside, Outside};

class GridXZ{
  private :

  enum NodeStatus {IGNORED, SETTLED, TO_COMPUTE};

  struct Node{
    float x,y,z; // 3D space
    int X, Z; // grid
    Node* left, * right, * up, * down;
    NodeStatus nodeStatus;
    Node(int,int);
  };
  
  int nbCellX;
  int nbCellZ;
  int nbrTO_COMPUTE;
  float dx;
  float dz;
  std::vector<Node> nodes;
  std::map<std::pair<int, int>, int> XZMappingToA;
  
  arma::fvec b;
  arma::fvec U;
  arma::fmat A;
  
  public :
  GridXZ(int,int,float,float,float,float);
  void generateLinks();
  void addingContour(int,int,int,int,Remove,float);
  void solve();
  void consoleOutput();
  void openglRender();
};

#endif // __GRIDXZ_H_INCLUDED__ 
