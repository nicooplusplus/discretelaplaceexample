/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *                                                                                  J'♥ SOPHIE
 */

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "OpenGL_X11_Stuffs.h"
#include "GridXZ.h"

int main(int argc, char **argv){
  CreateWindow();
  printHelp(); 
  
  cam.at(6.04995,2.68537,-0.76156);
  cam.to(5.34976,2.16638,-0.271274);
  
  GridXZ grid(/*deltas*/40,40,/*3D space coordinates*/ -2.0f,-2.0f,2.0f,2.0f);
  grid.addingContour(/*grid coordinates*/ 0, 0, 39, 39, /*points to remove*/ Outside, /*height of the contour*/ 0.0f);
  grid.addingContour(/*grid coordinates*/ 5, 5, 7, 7, /*points to remove*/ Inside, /*height of the contour*/ 1.5f);
  grid.addingContour(/*grid coordinates*/ 25, 35, 17, 17, /*points to remove*/ Inside, /*height of the contour*/ 0.8f);
  grid.generateLinks();
  grid.solve();
  
  while(true){
    ExposeFunc(win,cam); 
    grid.openglRender();
    if(Axis) Draw3DAxis();
    glXSwapBuffers(dpy, win); // swap baby swap
    
    usleep(1000);
    CheckKeyboard();
  }

	return 0;
}

