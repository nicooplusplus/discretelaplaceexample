/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *                                                                                  
 */
#ifndef __OPENGL_X11_STUFFS_H_INCLUDED__
#define __OPENGL_X11_STUFFS_H_INCLUDED__
#include<string.h>
#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#define GL_GLEXT_PROTOTYPES
#include<GL/glx.h>
#include<GL/glu.h>
#include<GL/glext.h>
#define PI 3.14159265


Display                 *dpy;
Window                  root, win;
GLint                   att[]   = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
XVisualInfo             *vi;
GLXContext              glc;
Colormap                cmap;
XSetWindowAttributes    swa;
XWindowAttributes       wa;
XEvent                  xev;


bool Axis = true;

struct quaternion{
  double x, y, z, w;
};

double length(quaternion quat){
  return sqrt(quat.x * quat.x + quat.y * quat.y +
              quat.z * quat.z + quat.w * quat.w);
};

quaternion normalize(quaternion quat){
  double L = length(quat);

  quat.x /= L;
  quat.y /= L;
  quat.z /= L;
  quat.w /= L;

  return quat;
};

quaternion conjugate(quaternion quat){
  quat.x = -quat.x;
  quat.y = -quat.y;
  quat.z = -quat.z;
  return quat;
};

quaternion mult(quaternion A, quaternion B){
  quaternion C;

  C.x = A.w*B.x + A.x*B.w + A.y*B.z - A.z*B.y;
  C.y = A.w*B.y - A.x*B.z + A.y*B.w + A.z*B.x;
  C.z = A.w*B.z + A.x*B.y - A.y*B.x + A.z*B.w;
  C.w = A.w*B.w - A.x*B.x - A.y*B.y - A.z*B.z;
  return C;
};

enum Axis {X, Y, Z};// only for convenience

enum Mvt{ FORWARD, BACKWARD, STRAFELEFT, STRAFERIGHT };

class Camera{
  public:
  GLdouble pos[3];     // xyz position where camera is located
  GLdouble dir[3];     // xyz position where camera is looked at
  GLdouble gazeV[3];   // view vector
  GLdouble rightV[3];  // right vector
  GLdouble upV[3];     // up vector

  Camera(){
    pos[X] = 2;
    pos[Y] = 2;
    pos[Z] = 2;
    dir[X] = 0;
    dir[Y] = 0;
    dir[Z] = 0;
    upV[X] = 0;
    upV[Y] = 1;
    upV[Z] = 0;
  }

  void at(double _x, double _y, double _z){
    pos[X]=_x, pos[Y]=_y, pos[Z]=_z;
  }
  
  void to(double _x, double _y, double _z){
    dir[X]=_x,dir[Y]=_y,dir[Z]=_z;
  }

  void calculateVectors() {
    gazeV[X] = dir[X] - pos[X];
    gazeV[Y] = dir[Y] - pos[Y];
    gazeV[Z] = dir[Z] - pos[Z];
    double mag = sqrt(gazeV[X]*gazeV[X]+gazeV[Y]*gazeV[Y]+gazeV[Z]*gazeV[Z]);
    gazeV[X] /= mag;        // normalize gaze vector
    gazeV[Y] /= mag;
    gazeV[Z] /= mag;

    rightV[X] = gazeV[Y]*upV[Z] - gazeV[Z]*upV[Y];   // rightV = gazeV x upV
    rightV[Y] = gazeV[Z]*upV[X] - gazeV[X]*upV[Z];
    rightV[Z] = gazeV[X]*upV[Y] - gazeV[Y]*upV[X];
    mag = sqrt(rightV[X]*rightV[X]+rightV[Y]*rightV[Y]+rightV[Z]*rightV[Z]);
    rightV[X] /= mag;
    rightV[Y] /= mag;
    rightV[Z] /= mag;
  }

  void rotation(double angle, double v[3], double w[3]) {
    quaternion temp, quat_view, result;
    
    temp.x = v[X] * sin(PI/360.*angle);
    temp.y = v[Y] * sin(PI/360.*angle);
    temp.z = v[Z] * sin(PI/360.*angle);
    temp.w = cos(PI/360.*angle);

    quat_view.x = gazeV[X];
    quat_view.y = gazeV[Y];
    quat_view.z = gazeV[Z];
    quat_view.w = 0;

    result = mult(mult(temp, quat_view), conjugate(temp));
    dir[X] = result.x+pos[X];
    dir[Y] = result.y+pos[Y];
    dir[Z] = result.z+pos[Z];
  }

  void Slide(int mvt ) {
    switch(mvt){
    case 0 :
    // forward
      pos[X] += gazeV[X];//*4.*DT;
      pos[Y] += gazeV[Y];//*4.*DT;
      pos[Z] += gazeV[Z];//*4.*DT;
      dir[X] += gazeV[X];//*4.*DT;
      dir[Y] += gazeV[Y];//*4.*DT;
      dir[Z] += gazeV[Z];//*4.*DT;
    break;

    case 1 :
    // backward
      pos[X] -= gazeV[X];//*4.*DT;
      pos[Y] -= gazeV[Y];//*4.*DT;
      pos[Z] -= gazeV[Z];//*4.*DT;
      dir[X] -= gazeV[X];//*4.*DT;
      dir[Y] -= gazeV[Y];//*4.*DT;
      dir[Z] -= gazeV[Z];//*4.*DT;
    break;

    case 2 :
    // strafe left
      pos[X] += rightV[X];//*4.*DT;
      pos[Y] += rightV[Y];//*4.*DT;
      pos[Z] += rightV[Z];//*4.*DT;
      dir[X] += rightV[X];//*4.*DT;
      dir[Y] += rightV[Y];//*4.*DT;
      dir[Z] += rightV[Z];//*4.*DT;
    break;

    case 3 :
    // strafe right
      pos[X] -= rightV[X];//*4.*DT;
      pos[Y] -= rightV[Y];//*4.*DT;
      pos[Z] -= rightV[Z];//*4.*DT;
      dir[X] -= rightV[X];//*4.*DT;
      dir[Y] -= rightV[Y];//*4.*DT;
      dir[Z] -= rightV[Z];//*4.*DT;
    break;

    }
  }

};
Camera cam;

void Draw3DAxis(){

  glLineWidth(1.5);

  glBegin(GL_LINES);

  // axe X :
  glColor3f(1.0, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(3., 0, 0);

  // axe Y :
  glColor3f(0, 1.0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 3., 0);

  // axe Z :
  glColor3f(0, 0, 1.0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 3.);
  glEnd();

  // grid :
  glLineWidth(1.0);
  glBegin(GL_LINES);
  glColor3f(.4,.4,.4);
  for(int i=-10;i<=10;i++) {
    glVertex3f(i,-0.001,-10);
    glVertex3f(i,-0.001,10);
    glVertex3f(-10,-0.001,i);
    glVertex3f(10,-0.001,i);
  }
  glEnd();
  glLineWidth(1.0);
}

void ExposeFunc(Window& window, const Camera& _cam){
  glXMakeCurrent(dpy, window, glc);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.25, 0.25, 0.25, 1.00);
  XGetWindowAttributes(dpy, window, &wa);
  glViewport(0, 0, wa.width, wa.height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(80, 1, 0.01, 1000000);
 
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  cam.calculateVectors();
  gluLookAt(_cam.pos[X], _cam.pos[Y], _cam.pos[Z], _cam.dir[X], _cam.dir[Y],_cam.dir[Z], 0, 1, 0);  


  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void CreateWindow(){
  if((dpy = XOpenDisplay(NULL)) == NULL) {
    exit(0);
  }

  root = DefaultRootWindow(dpy);
 
  if((vi = glXChooseVisual(dpy, 0, att)) == NULL) {
    exit(0);
  }
        
  if((cmap = XCreateColormap(dpy, root, vi->visual, AllocNone)) == 0) {
    exit(0);
  }
        
  swa.event_mask = KeyPressMask;
  swa.colormap   = cmap;
  win = XCreateWindow(dpy, root, 0, 0, 640, 480, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &swa);
  XStoreName(dpy, win, "Show No Mercy"); // Mad Max - Interceptor
  XMapWindow(dpy, win);

   if((glc = glXCreateContext(dpy, vi, NULL, GL_TRUE)) == NULL) {
     exit(0);
   }
}

void ExitProgram(){
  glXMakeCurrent(dpy, None, NULL);
  glXDestroyContext(dpy, glc);
  XDestroyWindow(dpy, win);
  XCloseDisplay(dpy);
  exit(0);
}

void printHelp(){
  std::cout << "   Escape : Exit program\n" 
            << "   Right,Left,Up,Down : camera translation in the view direction\n"
            << "   q,s,z,s : camera rotation (AZERTY keyboard!!) \n"
            << "   o : toggle 3D axis\n"
            << "   t,y : camera translate above y axis\n"
            << "   w,v : camera translate above x axis\n"
            << "   x,c : camera translate above z axis\n"
            << "   i : print camera position and direction\n"
            << "   h : guess what...\n";
}

void CheckKeyboard() {
  XNextEvent(dpy, &xev);
  switch( xev.type ){
  case KeyPress:
    char *key_string = XKeysymToString(XkbKeycodeToKeysym(dpy, xev.xkey.keycode, 0, 0));
    if(strncmp(key_string, "Escape", 5)     == 0) ExitProgram();
    else if(strncmp(key_string, "Right", 5) == 0) cam.Slide(STRAFERIGHT);
    else if(strncmp(key_string, "Left", 4)  == 0) cam.Slide(STRAFELEFT);
    else if(strncmp(key_string, "Up", 2)    == 0) cam.Slide(FORWARD);
    else if(strncmp(key_string, "Down", 4)  == 0) cam.Slide(BACKWARD);
    else if(strncmp(key_string, "q", 1)     == 0) { double y[3]={0,1,0}; cam.rotation( 4.,y,cam.rightV); }
    else if(strncmp(key_string, "d", 1)     == 0) { double y[3]={0,1,0}; cam.rotation(-4.,y,cam.rightV); }
    else if(strncmp(key_string, "z", 1)     == 0 && cam.gazeV[Y]< 0.99) cam.rotation(4.,cam.rightV,cam.upV); 
    else if(strncmp(key_string, "s", 1)     == 0 && cam.gazeV[Y]>-0.99) cam.rotation(-4.,cam.rightV,cam.upV);
    else if(strncmp(key_string, "o", 1)     == 0) { Axis = !Axis; }
    else if(strncmp(key_string, "h", 1)     == 0) { printHelp(); }
    else if(strncmp(key_string, "y", 1)     == 0) { cam.pos[Y]+=1.0; cam.dir[Y]+=1.0; }
    else if(strncmp(key_string, "t", 1)     == 0) { cam.pos[Y]-=1.0; cam.dir[Y]-=1.0; }
    else if(strncmp(key_string, "v", 1)     == 0) { cam.pos[X]+=0.1; cam.dir[X]+=0.1; }
    else if(strncmp(key_string, "w", 1)     == 0) { cam.pos[X]-=0.1; cam.dir[X]-=0.1; }
    else if(strncmp(key_string, "c", 1)     == 0) { cam.pos[Z]+=0.1; cam.dir[Z]+=0.1; }
    else if(strncmp(key_string, "x", 1)     == 0) { cam.pos[Z]-=0.1; cam.dir[Z]-=0.1; }
    else if(strncmp(key_string, "i", 1)     == 0) { std::cout << "cam.at(" << cam.pos[X] << "," << cam.pos[Y] << "," << cam.pos[Z] << ");\ncam.to(" << cam.dir[X] << "," << cam.dir[Y] << "," << cam.dir[Z] << ");" << std::endl;  }

  break;

  }

}

#endif // __OPENGL_X11_STUFFS_H_INCLUDED__ 

