/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *                                                                                  
 */
 
#include "GridXZ.h"

GridXZ::Node::Node( int _X, int _Z)
:X(_X), Z(_Z), left(NULL), right(NULL), up(NULL), down(NULL), nodeStatus(IGNORED), x(0), y(0), z(0){};

GridXZ::GridXZ(int _nbCellX, int _nbCellZ, float x1, float z1, float x2, float z2):nbCellX(_nbCellX),nbCellZ(_nbCellZ),nbrTO_COMPUTE(0){
  dx = fabs((x2-x1)/((float)nbCellX));
  dz = fabs((x2-z1)/((float)nbCellZ));
  if(dz<0.001 || dx<0.001) std::cout << "dx or dz <0.001"<<std::endl;
  nodes.reserve(nbCellX*nbCellZ);
  for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
    Node temp(X,Z);
    temp.x=X*dx;
    temp.z=Z*dz;
    nodes.push_back(temp);
  }
};

void GridXZ::generateLinks(){
  for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
    if(nodes[X*nbCellZ+Z].nodeStatus!=IGNORED){
      if(X>0 && nodes[(X-1)*nbCellZ+Z].nodeStatus!=IGNORED){
        nodes[X*nbCellZ+Z].left = &nodes[(X-1)*nbCellZ+Z];
        nodes[(X-1)*nbCellZ+Z].right = &nodes[X*nbCellZ+Z];
      }
      if(Z>0 && nodes[X*nbCellZ+(Z-1)].nodeStatus!=IGNORED){
        nodes[X*nbCellZ+Z].up = &nodes[X*nbCellZ+(Z-1)];
        nodes[X*nbCellZ+(Z-1)].down = &nodes[X*nbCellZ+Z];
      }
    }
  }
};

void GridXZ::addingContour(int X1, int Z1, int X2, int Z2, Remove InOut, float height){
  std::vector<Node> contour;
  contour.push_back(Node(X1,Z1));
  contour[0].y = height;
  contour.push_back(Node(X1,Z2));
  contour[1].y = height;
  contour.push_back(Node(X2,Z2));
  contour[2].y = height;
  contour.push_back(Node(X2,Z1));
  contour[3].y = height;
  contour.push_back(Node(X1,Z1)); // to make a loop
  contour[4].y = height;
  
  // sets the nodes status
  for(int i=1 ; i<contour.size() ; i++){
    /*distance*/ float D0 = sqrt(pow(contour[i-1].X - contour[i].X,2)+pow(contour[i-1].Z - contour[i].Z,2));

    for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
      /*distance*/   float D1 = sqrt(pow(contour[i-1].X - nodes[X*nbCellZ+Z].X,2)+pow(contour[i-1].Z - nodes[X*nbCellZ+Z].Z,2));
      /*distance*/   float D2 = sqrt(pow(contour[i].X - nodes[X*nbCellZ+Z].X,2)+pow(contour[i].Z - nodes[X*nbCellZ+Z].Z,2));
      
      // check if the node is on the contour :
      if(D1+D2<D0+0.0001f){
        /*set status*/ nodes[X*nbCellZ+Z].nodeStatus=SETTLED;
        /*set height*/ nodes[X*nbCellZ+Z].y=height;
      }else{
        // so the node isn't on the contour, check if is inside :
        bool inside=false;

        for (int j = 0, k = contour.size()-1; j < contour.size(); k = j++) {
          if ( ((contour[j].Z>nodes[X*nbCellZ+Z].Z) != (contour[k].Z>nodes[X*nbCellZ+Z].Z)) && (nodes[X*nbCellZ+Z].X < (contour[k].X-contour[j].X) * (contour[j].Z) / (contour[k].Z-contour[j].Z) + contour[j].X) )
            inside = !inside;
        }
        if(inside && nodes[X*nbCellZ+Z].nodeStatus!=SETTLED){
          nodes[X*nbCellZ+Z].nodeStatus= (InOut==Outside)? TO_COMPUTE : IGNORED;
        }
      }
    }
  }
};

void GridXZ::solve(){
  for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
    if(nodes[X*nbCellZ+Z].nodeStatus==TO_COMPUTE){
      XZMappingToA[std::make_pair(X,Z)]=nbrTO_COMPUTE;
      nbrTO_COMPUTE++;
    }
  }
  
  // we set the dimensions of vec and matrix
  b.set_size(nbrTO_COMPUTE);
  b.fill(0.0f);
  U.set_size(nbrTO_COMPUTE);
  U.fill(0.0f);
  A.set_size( nbrTO_COMPUTE , nbrTO_COMPUTE);
  A.fill(0.0f);
  
  int I=0; /*index on node TO_COMPUTE*/
  for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
    if(nodes[X*nbCellZ+Z].nodeStatus!=TO_COMPUTE) continue;
    A.at(I,XZMappingToA[std::make_pair(X,Z)])=4.0f;
    // up neibourer
    if(nodes[X*nbCellZ+Z].up){
      switch(nodes[X*nbCellZ+Z].up->nodeStatus){
        case SETTLED:
          b.at(I)+=nodes[X*nbCellZ+Z].up->y;
        break;
        
        case TO_COMPUTE:
          if(Z<nbrTO_COMPUTE) A.at(I,XZMappingToA[std::make_pair(X,Z-1)])=-1.0f;
        break;
        
        case IGNORED:
        break;
      }
    } // end of up
    // down neibourer
    if(nodes[X*nbCellZ+Z].down){
      switch(nodes[X*nbCellZ+Z].down->nodeStatus){
        case SETTLED:
          b.at(I)+=nodes[X*nbCellZ+Z].down->y;
        break;
        
        case TO_COMPUTE:           
          if(Z>0) A.at(I,XZMappingToA[std::make_pair(X,Z+1)])=-1.0f;
        break;
        
        case IGNORED:
        break;
      }
    } // end of down
    // left neibourer
    if(nodes[X*nbCellZ+Z].left){
      switch(nodes[X*nbCellZ+Z].left->nodeStatus){
        case SETTLED:
          b.at(I)+=nodes[X*nbCellZ+Z].left->y;
        break;
        
        case TO_COMPUTE:           
          if(X>0) A.at(I,XZMappingToA[std::make_pair(X-1,Z)])=-1.0f;
        break;
        
        case IGNORED:
        break;
      }
    } // end of left
    // right neibourer
    if(nodes[X*nbCellZ+Z].right){
      switch(nodes[X*nbCellZ+Z].right->nodeStatus){
        case SETTLED:
          b.at(I)+=nodes[X*nbCellZ+Z].right->y;
        break;
        
        case TO_COMPUTE:           
          if(X<nbrTO_COMPUTE) A.at(I,XZMappingToA[std::make_pair(X+1,Z)])=-1.0f;
        break;
        
        case IGNORED:
        break;
      }
    } // end of right
  I++;
  } // end of for
  U = arma::solve(A,b);
  
  // send results to nodes
  I=0;
  for(int Z=0 ; Z<nbCellZ ; Z++)for(int X=0 ; X<nbCellX ; X++){
    if(nodes[X*nbCellZ+Z].nodeStatus==TO_COMPUTE){
      nodes[X*nbCellZ+Z].y = U.at(I);
      nodes[X*nbCellZ+Z].nodeStatus = SETTLED;
      I++;
    }
  }
};

void GridXZ::consoleOutput(){
  std::cout<<std::endl;
  
  std::cout<<"nbrTO_COMPUTE:"<<nbrTO_COMPUTE<<std::endl;
  for (int i=0 ; i<nodes.size() ; i++){
    if(nodes[i].nodeStatus==IGNORED) continue;
    std::cout << "me("<<nodes[i].X<<","<<nodes[i].Z<<") status("<<nodes[i].nodeStatus<<") 3D("<<nodes[i].x<<","<<nodes[i].y<<","<<nodes[i].z;
    if(nodes[i].left) std::cout <<") left("<<nodes[i].left->X<<","<<nodes[i].left->Z;
    if(nodes[i].right) std::cout <<") right("<<nodes[i].right->X<<","<<nodes[i].right->Z;
    if(nodes[i].up) std::cout <<") up("<<nodes[i].up->X<<","<<nodes[i].up->Z;
    if(nodes[i].down) std::cout <<") down("<<nodes[i].down->X<<","<<nodes[i].down->Z;
    std::cout<<")"<<std::endl;
  }
  
  std::cout << A << std::endl;
  std::cout << b << std::endl;
  std::cout << U << std::endl;
  for(int Z=0 ; Z<nbCellZ ; Z++){
    for(int X=0 ; X<nbCellX ; X++){
    std::cout<<"("<<X<<","<<Z<<")="<<nodes[X*nbCellZ+Z].y<<"|";
    }
    std::cout<<std::endl;
  }
};
