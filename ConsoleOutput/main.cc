/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 *                                                                                  J'♥ SOPHIE
 */

#include "GridXZ.h"

int main(int argc, char **argv)
{
  std::cout << "example from : http://en.wikipedia.org/wiki/Discrete_Poisson_equation\n";
  std::cout << "(note : for Laplace g=0)" << std::endl;
  GridXZ grid(/*deltas*/5,5,/*3D space coordinates*/ -1.0f,-1.0f,1.0f,1.0f);
  grid.addingContour(/*grid coordinates*/ 0, 0, 4, 4, /*points to remove*/ Outside, /*height of the contour*/ -0.2f);
  grid.generateLinks();
  grid.solve();
  grid.consoleOutput();

  return 0;
}

